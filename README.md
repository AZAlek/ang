**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

by Aleksey Zhadeev
for Front-End Frameworks: Angular 4.0 by Hong Kong University of Science and Technology (Coursera.org).
---

Description

The project contains two projects.  Front-end application, as well as json-server that acts as a dummy back-end.
-Front-end application requires Angular CLI. Install dependencies from package.json (command prompt-> npm install)
-To install json-server -> npm install json-server -g.
 
Once installed:

-run json-server before starting app server -> json-server --watch db.json (will run mock back-end on localhost:3000)
-run app server -> ng serve --open (will open app on localhost:4200 in default browser)






