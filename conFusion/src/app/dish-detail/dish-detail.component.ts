import { Component, OnInit, Inject } from '@angular/core';
import {Params, ActivatedRoute} from '@angular/router';
import {Location, DatePipe} from '@angular/common';
import { Dish } from '../shared/dish';
import {DishService} from '../services/dish.service';
import {Comment} from '../shared/comment';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import {visibility, flyInOut, expand} from '../animations/app.animation';
import 'rxjs/add/operator/switchMap';


@Component({
  selector: 'app-dish-detail',
  providers:[DatePipe],
  templateUrl: './dish-detail.component.html',
  styleUrls: ['./dish-detail.component.scss'],
  host : {
      '[@flyInOut]': 'true',
      'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})

export class DishDetailComponent implements OnInit {

  dish: Dish;
  dishcopy=null;
  dishIds: number[];
  prev:number;
  next: number;
  reviewForm: FormGroup;
  review:Comment;
errMess:string;
visibility = 'shown';


      formErrors= {
        'author':'',
        'comment': '',
        };
        
        validationMessages={
        'author': {
        'required': 'Name is required',
        'minlength': 'Name must be at least 2 characters long',
        'maxlength': 'Name cannot be more than 25 characters long'
        },
        'comment': {
        'required': 'Comment is required',
        'minlength': 'Comment must be at least 2 characters long',
        },        
        }

    


  constructor(private dishservice: DishService,
  private route: ActivatedRoute,
  private location: Location,
  private fb: FormBuilder,
  private datePipe:DatePipe,
  @Inject('BaseURL') private BaseURL) { }

  ngOnInit() {
    this.createForm();
    this.dishservice.getDishIds().subscribe((dishIds) => {this.dishIds = dishIds; });
    this.route.params
      .switchMap((params:Params) => {this.visibility= 'hidden'; return this.dishservice.getDish(+params['id']); })
      .subscribe(dish => { this.dish = dish; this.dishcopy=dish; this.setPrevNext(dish.id); this.visibility= 'shown;'  },
    errmess=>this.errMess=<any> errmess);
     
  }

  setPrevNext(dishId: number) {
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
  }

  goBack(): void{
    this.location.back();
  }

  createForm(){
    this.reviewForm = this.fb.group({
    author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25) ]],
    comment: ['', [Validators.required, Validators.minLength(2)]],
    rating: '5',
    
    });

    this.reviewForm.valueChanges.subscribe(data=> this.onValueChanged(data) );
    this.onValueChanged(); 
  };


  onValueChanged(data?: any){
    if(!this.reviewForm){
      return;
    }
    const form=this.reviewForm;
    for (const field in this.formErrors){
      this.formErrors[field]= '';
      const control = form.get(field);
      if (control && control.dirty && ! control.valid){
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field]+= messages[key] +  ' ';
        }


      }
    }
  }

  onSubmit(date){
    this.review={
      
      author: this.reviewForm.value.author,
      rating: this.reviewForm.value.rating,
      comment: this.reviewForm.value.comment,
      date: this.datePipe.transform(Date.now(), 'MMM d, y').toString() 
    }
      this.dishcopy.comments.push(this.review);
      this.dishcopy.save().subscribe(dish=> this.dish=dish);
    console.log(this.review.author);
    console.log(this.review.rating);
    console.log(this.review.comment);
    console.log(this.review.date);
this.dish.comments.push(this.review);

    this.reviewForm.reset({
      author:'',
      comment: '',
      rating: ''
    });
  }

}
