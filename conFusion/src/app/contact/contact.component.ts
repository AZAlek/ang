import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Feedback, ContactType} from '../shared/feedback';
import {flyInOut, visibility, expand} from '../animations/app.animation';
import { FeedbackService } from '../services/feedback.service';
import {Params, ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/toPromise';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host : {
      '[@flyInOut]': 'true',
      'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})
export class ContactComponent implements OnInit {
errMess:string;
feedbackForm: FormGroup;
feedback: Feedback;
fbcopy: Feedback;
contactType = ContactType;
postReturn:number;
visibility:string;
vis:boolean;
res:boolean;

formErrors= {
'firstname':'',
'lastname': '',
'telnum': '',
'email': '',
};

validationMessages={
'firstname': {
'required': 'First Name is required',
'minlength': 'First Name must be at least 2 characters long',
'maxlength': 'First Name cannot be more than 25 characters long'
},
'lastname': {
'required': 'Last Name is required',
'minlength': 'Last Name must be at least 2 characters long',
'maxlength': 'Last Name cannot be more than 25 characters long'
},
'telnum': {
  'required': 'Tel. Number is required',
  'pattern': 'Tel. number must contain only numbers.'
},
'email': {
  'required': 'Email is required',
  'email': 'Email not in valid format'
}

}

  constructor(private fb: FormBuilder, private feedbackservice: FeedbackService, private route: ActivatedRoute ) { 
    this.createForm();
  }

  ngOnInit() {
    this.vis=true;
    this.res=false;
  }

  createForm(){
this.feedbackForm = this.fb.group({
firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25) ] ],
lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25) ]],
telnum: [0, [Validators.required, Validators.pattern ]],
email: ['', [Validators.required, Validators.email ] ] ,
agree: false,
contacttype: 'None',
message: '',
});

this.feedbackForm.valueChanges.subscribe(data=> this.onValueChanged(data) );
this.onValueChanged(); // (re)set form validation messages 
  }

  onValueChanged(data?: any){
    if(!this.feedbackForm){
      return;
    }
    const form=this.feedbackForm;
    for (const field in this.formErrors){
      this.formErrors[field]= '';
      const control = form.get(field);
      if (control && control.dirty && ! control.valid){
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field]+= messages[key] +  ' ';
        }
      }
    }
  }

  miStuff():boolean{
    if(this.res==false && this.vis==false){
return true;
    }
    else{
      return false;
    }
  }
  onSubmit(){
    this.feedback= this.feedbackForm.value;
    let fbcopy=this.feedback;
    this.visibility='hidden';
    this.vis=false;
    this.feedbackservice.submitFeedback(fbcopy).subscribe(
 
      fbcopy=> { console.log(fbcopy); 
      
      
      return new Promise(resolve=> {
        this.res=true;
        this.feedback=fbcopy;
  
          setTimeout(() => { 
          
            resolve(); console.log("resolved") }, 5000
      )}
    ).then(()=>{ 
      
      this.visibility= 'shown';
      this.res=false;this.vis=true; 
  
      this.feedbackForm.reset({
        firstname:'',
        lastname: '',
        telnum: '',
        email: '',
        agree: false,
        contacttype: 'None',
        message : ''
      });
      

  }) });
  }

}
